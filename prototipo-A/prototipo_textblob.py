from textblob import TextBlob
import matplotlib.pyplot as plt
from collections import Counter
import pandas as pd
import re

def limpiarTexto(texto):
    patron = re.compile(r'[\U0001F600-\U0001F64F\U0001F300-\U0001F5FF\U0001F680-\U0001F6FF\U0001F700-\U0001F77F\U0001F780-\U0001F7FF\U0001F800-\U0001F8FF\U0001F900-\U0001F9FF\U0001FA00-\U0001FA6F\U0001FA70-\U0001FAFF\U00002702-\U000027B0\U000024C2-\U0001F251]+|[\'\"\u2018-\u201D]+', flags=re.UNICODE)
    texto = re.sub('🍎','Apple', texto)
    texto = re.sub(patron,'',texto)
    return texto

data = [
        "Horrible capitalist company. Refused to replace my Apple Watch battery for $100+ claiming that it had a water leak in it, even though all parties knew that their own advertising shows people swimming with Apple Watches and that everyone does it. So if you're hoping to ever pay to replace your battery, know that you probably should never swim with it!",
        "Just swung by the Apple Store, and man, it was awesome! The staff were super cool, helped me out big time, and the store had this cool vibe. Got to play around with the gadgets, and the checkout was a breeze. If you're into top-notch tech and a laid-back shopping experience, hit up the Apple Store. Totally recommend it! ??",
        "Ironically, I am an iOS user, but let me be honest and say that all Apple is selling is their name value. If youre asking me to talk about functionalities, Apple gadgets are absolutely NOT the answer.Theres a saying that where theres a price, theres value, but frankly I dont think that applies for Apples products. Unnecessarily complicated, overpriced, typical, and uninspired. I personally dont think theres anything innovative or game-changing about Apples products. Its surely selling luxury, but luxury is not equal to top-notch quality/functionality. Unless luxurys all you care for then you do you!",
        "A few years ago ﻿I bought an iMac Computer from 🍎 and received ""free"" Beats earphones/earbuds. I didn't unpack them right away thinking they wouldn't fit because that shape doesn't fit my ears and was planning to regift them. When they were finally taken out of the sealed box at Christmas, we found out that they will charge but are NOT discoverable in Bluetooth. I did a search and found dozens of other consumers had the same problem with those models (some said theirs stopped working within 6 months to a year). I called 🍏 and was told the item is extinct (came out late 2016...years before I got them) and therefore cannot be repaired or replaced. You would think since 🍎 bought that company they would have a better, reliable product even if it was free or is that why it was free? So the next time you are offered a ""free"" with purchase item from them, open the box right away to see if you too received a dud."
        ]

polaridades = []
indicador = []

dato = input("Introduce una frase para interactuar con el prototipo (EN/ES)")
data.append(dato)

# Creamos un dataframe con el array de reviews y le aplicamos el método limpiarTexto() creado anteriormente, para preparar el texto
df = pd.DataFrame(data,columns=["Reviews"])
df["Reviews"] = df["Reviews"].apply(limpiarTexto)

# Aplicamos la funcionalidad de TextBlob para calcular la polaridad de cada comentario
for dato in df["Reviews"]:
    polaridad = TextBlob(dato).sentiment.polarity
    polaridades.append(str(polaridad))

df["Polaridad"] = polaridades

# Clasificamos los sentimientos según su polaridad, y los añadimos al dataframe
for dato in df["Polaridad"]:
    numero = float(dato)
    if(numero < 0):
        indicador.append(-1)
    else:
        if(numero == 0):
            indicador.append(0)
        else:
            indicador.append(1)

df["Indicador"] = indicador

# Creamos un objeto contador, de la clase collections
contador = Counter(indicador)

# Mostramos los resultados calificados en el análisis, siendo -1 valor negativo, 0 valor neutro y 1 valor positivo
plt.bar(contador.keys(),contador.values(), color='skyblue')
plt.xlabel('Estados')
plt.ylabel('Frecuencia')
plt.title('Análisis de Sentimiento')

plt.grid(axis='y')
plt.show()


